#include "Options.h"

QList<QString> Options::GetAutoConnectList()
{
    return autoConnectList;
}

void Options::AddToAutoConnect(QString server)
{
    autoConnectList << server;
}

void Options::DelFromAutoConnect(QString server)
{
    autoConnectList.removeOne(server);
}
