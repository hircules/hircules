#ifndef USER_LIST
#define USER_LIST

#include <QWidget>
#include <QListWidget>
#include <QString>
#include <QList>

class UserList : public QListWidget
{
    public:
        UserList(QWidget *parent = 0);
        ~UserList();
    private:
        QList<QString> userList;
};

#endif
