#include "Channel.h"

Channel::Channel(QString name, QString modes, QMap<QString, User*> users) :
userList(users), channelName(name), channelModes(modes)
{}

void Channel::AddUser(User *newUser)
{
    userList.insert(newUser->GetUserNick(), newUser);
}

void Channel::RemoveUser(User *leavingUser)
{
    userList.remove(leavingUser->GetUserNick());
}

QMap<QString, User*> Channel::GetUserList()
{
    return userList;
}

QString Channel::GetName()
{
    return channelName;
}

void Channel::SetName(QString newName)
{
    channelName = newName;
}

QString Channel::GetModes()
{
    return channelModes;
}

void Channel::SetModes(QString newModes)
{
    channelModes = newModes;
}
