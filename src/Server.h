#ifndef SERVER
#define SERVER

#include "ChatInstance.h"

class Server
{
    public:
        Server(QString address="", int port=0);
        QMap<QString, ChatInstance*> GetChatInstanceList();
        QMap<QString, User*> GetKnownUserList();
        void AddChatInstance(ChatInstance *newChatInstance);
        void RemoveChatInstance(ChatInstance *leftChatInstance);
        QString GetAddress();
        void SetAddress(QString newAddress);
        int GetPort();
        void SetPort(int newPort);

    private:
        QMap<QString, ChatInstance*> chatInstanceList;
        QMap<QString, User*> knownUsers;
        QString serverAddress;
        int serverPort;
};

#endif
