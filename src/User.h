#ifndef USER
#define USER

#include <QString>

class User {
    public:
        User(QString nick="", QString privs="", QString hostmask="");
        QString GetUserNick();
        void SetUserNick(QString newNick);
        QString GetPrivs();
        void SetPrivs(QString newPrivs);
        QString GetHostmask();
        void SetHostmask(QString newHostmask);

    private:
        QString userNick;
        QString userPriveleges;
        QString userHostmask;
};

#endif
