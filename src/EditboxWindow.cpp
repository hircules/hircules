#include <QWidget>
#include <QString>
#include <QGridLayout>
#include "EditboxWindow.h"

EditboxWindow::EditboxWindow(QWidget *parent)
    : QWidget(parent)
{
    editbox = new Editbox(this);
    contentPane = new ContentPane(this);
    layout = new QGridLayout;
    layout->addWidget(contentPane,0,0);
    layout->addWidget(editbox,1,0,1,2);
    setLayout(layout);    
}

EditboxWindow::~EditboxWindow()
{

}

void EditboxWindow::clearEditbox()
{
}

QString EditboxWindow::getEditboxText()
{
}

QString EditboxWindow::getContent()
{
}

void EditboxWindow::addLine(QString contentLine)
{
}

void EditboxWindow::setContent(QString content)
{
}
