#ifndef WINDOW_PANE
#define WINDOW_PANE

#include <QWidget>
#include <QMdiArea>
#include <QMdiSubWindow>

class WindowPane : public QMdiArea
{
    public:
        WindowPane(QWidget *parent = 0);
        ~WindowPane();

};

#endif
