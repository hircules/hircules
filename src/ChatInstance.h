#ifndef CHAT_INSTANCE
#define CHAT_INSTANCE

#include <QList>
#include <QString>
#include <QMap>
#include "User.h"

class ChatInstance
{
    public:
        void AddLine(QString newLine);
        QString GetTitle();
        void SetTitle(QString newTitle);
        virtual void AddUser(User *newUser) =0;
        virtual void RemoveUser(User *leavingUser) =0;
        virtual QMap<QString, User*> GetUserList() =0;

    private:
        QList<QString> chatText;
        QString chatTitle;
};

#endif
