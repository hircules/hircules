#ifndef CONTENT_PANE
#define CONTENT_PANE

#include <QWidget>
#include <QTextEdit>

class ContentPane : public QTextEdit
{
    public:
        ContentPane(QWidget *parent = 0);
        ~ContentPane();
};

#endif
