#include "ChannelWindow.h"
#include "UserList.h"
#include <QSplitter>

ChannelWindow::ChannelWindow(QWidget *parent)
    : EditboxWindow(parent)
{
    userList = new UserList(this);
    QSplitter *splitter = new QSplitter(this);
    splitter->addWidget(contentPane);
    splitter->addWidget(userList);
    splitter->setCollapsible(0,0);
    layout->addWidget(splitter,0,1);
    setLayout(layout);
}

ChannelWindow::~ChannelWindow()
{
    delete userList;
}

QList<QString> ChannelWindow::getUserList()
{
}
