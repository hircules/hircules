#ifndef CLIENT_WINDOW
#define CLIENT_WINDOW

#include <QMainWindow>
#include <QMenu>
#include "ServerWindow.h"
#include "WindowPane.h"
#include "TreeView.h"

class ClientWindow : public QMainWindow
{
    public:
        ClientWindow(QWidget *parent = 0);
        ~ClientWindow();
        
        void createMenus();
        void createTreeView();
    private:
        ServerWindow *defaultWindow;
        WindowPane *windowArea;
        TreeView *treeView;
        QMenu *fileMenu;
        QString windowTitle;
};

#endif
