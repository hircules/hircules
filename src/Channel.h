#ifndef CHANNEL
#define CHANNEL

#include "ChatInstance.h"

class Channel : public ChatInstance
{
    public:
        Channel(QString name, QString modes, QMap<QString, User*> users);
        void AddUser(User *newUser);
        void RemoveUser(User *leavingUser);
        QMap<QString, User*> GetUserList();
        QString GetName();
        void SetName(QString newName);
        QString GetModes();
        void SetModes(QString newModes);

    private:
        QMap<QString, User*> userList;
        QString channelName;
        QString channelModes;
};

#endif
