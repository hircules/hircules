#ifndef EDITBOX_WINDOW
#define EDITBOX_WINDOW

#include <QWidget>
#include <QMdiSubWindow>
#include <QString>
#include <QGridLayout>
#include "Editbox.h"
#include "ContentPane.h"

class EditboxWindow : public QWidget
{
    public:
        EditboxWindow(QWidget *parent = 0);
        ~EditboxWindow();

        void clearEditbox();
        QString getEditboxText();
        QString getContent();
        void addLine(QString contentLine);
        void setContent(QString content);
    protected:
        QString content;
        QGridLayout *layout;
        Editbox *editbox;
        ContentPane *contentPane;
};

#endif
