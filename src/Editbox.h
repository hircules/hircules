#ifndef EDITBOX
#define EDITBOX

#include <QWidget>
#include <QLineEdit>

class Editbox : public QLineEdit
{
    public:
        Editbox(QWidget *parent = 0);
        ~Editbox();
};

#endif
