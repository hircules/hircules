#ifndef OPTIONS
#define OPTIONS

#include <QString>
#include <QList>

class Options
{
    public:
        QList<QString> GetAutoConnectList();
        void AddToAutoConnect(QString server);
        void DelFromAutoConnect(QString server);

    private:
        QList<QString> autoConnectList;
};

#endif
