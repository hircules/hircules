#include "LocalUser.h"

LocalUser::LocalUser(QString realName, QString email, QString address,
        QString nick, QString privs, QString hostmask) :
User(nick, privs, hostmask),
userRealName(realName), userEmail(email), userAddress(address)
{}

QString LocalUser::GetUserRealName()
{
    return userRealName;
}

void LocalUser::SetUserRealName(QString newRealName)
{
    userRealName=newRealName;
}

QString LocalUser::GetUserEmail()
{
    return userEmail;
}

void LocalUser::SetUserEmail(QString newEmail)
{
    userEmail = newEmail;
}

QString LocalUser::GetUserAddress()
{
    return userAddress;
}

void LocalUser::SetUserAddress(QString newAddress)
{
    userAddress = newAddress;
}
