#include "Client.h"

Client::Client():
    instance(0)
{
}

QMap<QString, Server*> Client::GetServerList()
{
    return clientServerList;
}

void Client::AddServer(Server* newServer)
{
    clientServerList.insert(newServer->GetAddress(), newServer);
}

void Client::DelServer(Server* leftServer)
{
   clientServerList.remove(leftServer->GetAddress()); 
}

Client *Client::getInstance()
{
    if (instance == 0)
        instance = new Client();
    return instance;
}

LocalUser *Client::getLocalUserInfo()
{
}

ChatInstance *Client::getChatInstance(QString chatTitle, QString server)
{
    return clientServerList[server]->GetChatInstanceList()[chatTitle];
}

Server *Client::getServer(QString server)
{
    return clientServerList[server];
}

User *Client::getUser(QString nickname, QString server)
{
    return clientServerList[server]->GetKnownUserList()[nickname];
}
