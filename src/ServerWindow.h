#ifndef SERVER_WINDOW
#define SERVER_WINDOW

#include <QWidget>
#include "EditboxWindow.h"

class ServerWindow : public EditboxWindow
{
    public:
        ServerWindow(QWidget *parent = 0);
        ~ServerWindow();
};

#endif
