#ifndef CHANNEL_WINDOW
#define CHANNEL_WINDOW

#include <QWidget>
#include <QString>
#include <QList>
#include "EditboxWindow.h"
#include "UserList.h"

class ChannelWindow : public EditboxWindow
{
    public:
        ChannelWindow(QWidget *parent = 0);
        ~ChannelWindow();

        QList<QString> getUserList();
    private:
        UserList *userList;
};

#endif
