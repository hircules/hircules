#include "ChatInstance.h"

void ChatInstance::AddLine(QString newLine)
{
    chatText << newLine;
}

QString ChatInstance::GetTitle()
{
    return chatTitle;
}

void ChatInstance::SetTitle(QString newTitle)
{
    chatTitle = newTitle;
}
