#include "PrivateMessage.h"

void PrivateMessage::AddUser(User *newUser)
{
    userList.insert(newUser->GetUserNick(), newUser);
}

void PrivateMessage::RemoveUser(User *leavingUser)
{
    userList.remove(leavingUser->GetUserNick());
}

QMap<QString, User*> PrivateMessage::GetUserList()
{
    return userList;
}
