#include "User.h"

User::User(QString nick, QString privs, QString hostmask) :
userNick(nick), userPriveleges(privs), userHostmask(hostmask)
{}

QString User::GetUserNick()
{
    return userNick;
}

void User::SetUserNick(QString newNick)
{
    userNick = newNick;
}

QString User::GetPrivs()
{
    return userPriveleges;
}

void User::SetPrivs(QString newPrivs)
{
    userPriveleges = newPrivs;
}

QString User::GetHostmask()
{
    return userHostmask;
}

void User::SetHostmask(QString newHostmask)
{
    userHostmask = newHostmask;
}
