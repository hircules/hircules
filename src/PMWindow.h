#ifndef PM_WINDOW
#define PM_WINDOW

#include <QWidget>
#include "EditboxWindow.h"

class PMWindow : public EditboxWindow
{
    public:
        PMWindow(QWidget *parent = 0);
        ~PMWindow();
};

#endif
