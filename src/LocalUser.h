#ifndef LOCAL_USER
#define LOCAL_USER

#include <QString>
#include "User.h"

class LocalUser : public User
{
    public:
        LocalUser(QString realName, QString email, QString address,
                QString nick="", QString privs="", QString hostmask="");
        QString GetUserRealName();
        void SetUserRealName(QString newRealName);
        QString GetUserEmail();
        void SetUserEmail(QString newEmail);
        QString GetUserAddress();
        void SetUserAddress(QString newAddress);

    private:
        QString userRealName;
        QString userEmail;
        QString userAddress;
        
};

#endif
