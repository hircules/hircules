#ifndef PRIVATE_MESSAGE
#define PRIVATE_MESSAGE

#include "ChatInstance.h"

class PrivateMessage : public ChatInstance
{
    public:
        void AddUser(User *otherUser);
        void RemoveUser(User *leavingUser);
        QMap<QString, User*> GetUserList();

    private:
        QMap<QString, User*> userList;
};

#endif
