#include "Server.h"

Server::Server(QString address, int port) :
serverAddress(address), serverPort(port)
{}

QMap<QString, ChatInstance*> Server::GetChatInstanceList()
{
    return chatInstanceList;
}

QMap<QString, User*> Server::GetKnownUserList()
{
    return knownUsers;
}

void Server::AddChatInstance(ChatInstance *newChatInstance)
{
    chatInstanceList.insert(newChatInstance->GetTitle(), newChatInstance);
    knownUsers.unite(newChatInstance->GetUserList());
}

void Server::RemoveChatInstance(ChatInstance *leftChatInstance)
{
    chatInstanceList.remove(leftChatInstance->GetTitle());
}

QString Server::GetAddress()
{
    return serverAddress;
}

void Server::SetAddress(QString newAddress)
{
    serverAddress = newAddress;
}

int Server::GetPort()
{
    return serverPort;
}

void Server::SetPort(int newPort)
{
    serverPort = newPort;
}
