#include <QMenuBar>
#include <QDockWidget>
#include "ClientWindow.h"
#include "WindowPane.h"
#include "ChannelWindow.h"

ClientWindow::ClientWindow(QWidget *parent)
    : QMainWindow(parent)
{   
    createMenus();
    createTreeView();
    resize(800,600);
    windowTitle = "hIRCules";
    setWindowTitle(windowTitle);

    defaultWindow = new ServerWindow(this);
    windowArea = new WindowPane(this);
    windowArea->addSubWindow(defaultWindow);
    windowArea->addSubWindow(new ChannelWindow(this));
    setCentralWidget(windowArea);
}

ClientWindow::~ClientWindow()
{
    delete defaultWindow;
}

void ClientWindow::createTreeView()
{
    QDockWidget *dockWidget = new QDockWidget(tr("Treebar"),this);
    treeView = new TreeView;
    dockWidget->setWidget(treeView);
    dockWidget->resize(150,600);
    addDockWidget(Qt::LeftDockWidgetArea, dockWidget);
}
void ClientWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(tr("&File"));
}
