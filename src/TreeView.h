#ifndef TREE_VIEW
#define TREE_VIEW

#include <QTreeView>

class TreeView : public QTreeView
{
    public:
        TreeView();
        ~TreeView();

        /* Unsure on this, creates a lot of coupling
        void addServer(ServerWindow *serv);
        void addChannel(ChannelWindow *chan, ServerWindow *parent);
        */
};

#endif
