#ifndef CLIENT
#define CLIENT

#include "Server.h"
#include "LocalUser.h"
#include "Channel.h"
#include "User.h"
#include "PrivateMessage.h"
#include <QString>
#include <QMap>

class Client
{
    public:
        QMap<QString, Server*> GetServerList();
        void AddServer(Server *newServer);
        void DelServer(Server *leftServer);
        Client *getInstance();

        LocalUser *getLocalUserInfo();
        ChatInstance *getChatInstance(QString chatTitle, QString server);
        Server *getServer(QString server);
        User *getUser(QString nickname, QString server);
        PrivateMessage *getPrivateMessage(QString nickname, QString server);

    private:
        Client *instance;
        Client();
        QMap<QString, Server*> clientServerList;

        
};

#endif
